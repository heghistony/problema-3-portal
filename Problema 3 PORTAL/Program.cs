﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _4Probleme
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Preluare numar utilizator: ");
            int n = int.Parse(Console.ReadLine());
            int[] v = new int[n];
            Console.WriteLine("Introduceti numerele din vector: ");
            for (int i = 0; i < v.Length; i++)
            {
                v[i] = int.Parse(Console.ReadLine());
            }
            Console.WriteLine("Introduceti valorile a si b: ");
            int a = int.Parse(Console.ReadLine());
            int b = int.Parse(Console.ReadLine());
            int k = 0;

            for (int i = 0; i < v.Length; i++)
            {
                if (v[i] < a || v[i] > b)
                    k++;
            }

            int[] dif = new int[k];
            int x = 0;
            Console.WriteLine("Valorile care nu fac parte din interval sunt: ");
            while (v[x] < a || v[x] > b)
            {
                dif[x] = v[x];
                Console.WriteLine(dif[x]);
                x++;
                if (x == k)
                    break;
            }
            Console.WriteLine($"Ele sunt in numar de {x} ");

            Console.ReadKey();
        }
    }
}
